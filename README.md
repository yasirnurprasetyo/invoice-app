# Invoice-App

Simple invoice application included feature: 
1. CRUD data invoice
2. Print invoice

## Tech Stack
1. Laravel (PHP)
2. Javascript
3. Bootstrap
4. VueJs
5. MySQL

## Install

1. **Clone Repository**

```bash
git clone https://gitlab.com/yasirnurprasetyo/invoice-app.git
cd invoice-app
composer install
npm install
cp .env.example .env
```

2. **Buka `.env` lalu ubah baris berikut sesuai dengan databasemu yang ingin dipakai**

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```

3. **Instalasi website**

```bash
php artisan key:generate
php artisan migrate --seed
```

4. **Jalankan website**

```bash
php artisan serve
```

## Author

- LinkedIn : <a href="https://www.linkedin.com/in/yasir-nur-prasetya-03a035184/"> Yasir Nur Prasetya</a>