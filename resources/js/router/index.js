import { createRouter, createWebHistory } from "vue-router";

import InvoiceIndex from '../components/invoice/index.vue'
import InvoiceNew from '../components/invoice/new.vue'
import InvoiceShow from '../components/invoice/show.vue'
import InvoceEdit from '../components/invoice/edit.vue'

import NotFound from '../components/NotFound.vue'

const routes = [
    {
        path: '/',
        component: InvoiceIndex
    },
    {
        path: '/invoice/new',
        component: InvoiceNew
    },
    {
        path: '/:pathMatch(.*)*',
        component: NotFound
    },
    {
        path: '/invoice/show/:id',
        component: InvoiceShow,
        props:true
    },
    {
        path: '/invoice/edit/:id',
        component: InvoceEdit,
        props:true
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router